﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManager{
    private const string HOTSPOT_TYPE_TRAVEL = "destination";
    private const string HOTSPOT_TYPE_DESCRIPTION = "description";
    private const string HOTSPOT_TYPE_AUDIO = "audio";
    private JSONReader jsonReader;
    private ViewerBehaviour viewBehaviour;
    private HotSpotBehaviour hotSpotBehaviour;
    private MediaInfo mediaInfo;
    private LocationInfo currentLocation;

    public ControlManager(){
        jsonReader = new JSONReader();
        viewBehaviour = new ViewerBehaviour();
        hotSpotBehaviour = new HotSpotBehaviour();
        mediaInfo = new MediaInfo();
        GetMedia();
        currentLocation = mediaInfo.locations[0];
    }

    public void GetMedia(){
        // gets the media info from the JSON file
        mediaInfo = jsonReader.Read();
    }

    public string GetCurrentLocationPath(int locId){
        // sends media information to ViewerBehaviour
        // e.g: ViewerBehaviour.GetCurrentLocation(media);
        // Get location and return byte[]?
        // wait for locationName return
        // return that image path
        currentLocation = mediaInfo.locations[locId];
        return viewBehaviour.GetLocationPath(currentLocation);
    }

    public LocationInfo GetLocation (int id) {
        return mediaInfo.locations[id];
    }

    public string GetHotSpotType(ObjectInfo targetObject){
        // Returns the type of hotspot the target is
        if (targetObject.destinationId != -1){
            return HOTSPOT_TYPE_TRAVEL;
        }
        else if (targetObject.description != null){
            return HOTSPOT_TYPE_DESCRIPTION;
        } 
        else if (targetObject.audioFilePath != null){
            return HOTSPOT_TYPE_AUDIO;
        }
        else{
            Debug.Log(string.Format("No type found!: {0}; {1}; {2}",
                targetObject.destinationId.ToString(),
                targetObject.description,
                targetObject.audioFilePath));
            throw new NullReferenceException();
        }
    }

    public KeyValuePair<string, string> HotSpotActivate(ObjectInfo obj){
        // in total: should check if its for travel or information or audio
        // this: checks whatt type
        // hotspotbehaviour handles and returns command(?)
        if (obj.destinationId != -1){
            Debug.Log("TRAVELLING");
            string key = HOTSPOT_TYPE_TRAVEL;
            string val = GetCurrentLocationPath(obj.destinationId);

            return new KeyValuePair<string, string>(key, val);
        }
        else if (obj.description != null){
            Debug.Log("DESCRIPTION FOUND");
            string key = HOTSPOT_TYPE_DESCRIPTION;
            string val = "";

            return new KeyValuePair<string, string>(key, val);
        }
        else if (obj.audioFilePath != null){
            Debug.Log("AUDIO FOUND");
            string key = HOTSPOT_TYPE_AUDIO;
            string val = hotSpotBehaviour.ActivateAudio(obj);

            return new KeyValuePair<string, string>(key, val);
        }
        else{
            Debug.Log("ERROR");
            Debug.Log(obj.description);
            Debug.Log(obj.destinationId);
            Debug.Log(obj.audioFilePath);
            throw new NullReferenceException();
        }
    }

    public List<ObjectInfo> GetHotSpots(){
        // get all objectInfos from media
        // add them all to a list of ObjectInfos
        // return that list
        return currentLocation.objects;
    }
}
