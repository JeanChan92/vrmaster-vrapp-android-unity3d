﻿using System.Collections.Generic;

public class LocationInfo
{
	public int id { get; set; }
    public string title { get; set; }
    public string type { get; set; }
    public string subPath { get; set; }
    public string locationFilePath { get; set; }
    public int showTime { get; set; }
    public List<ObjectInfo> objects { get; set; }
}
