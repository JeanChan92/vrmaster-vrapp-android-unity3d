﻿using System.Collections.Generic;

public class ObjectInfo
{
    public string label;
    public string description;
    public string iconTag;
    public string imageFilePath;
    public string audioFilePath;
    public int destinationId;
    public PositionInfo position;
}
