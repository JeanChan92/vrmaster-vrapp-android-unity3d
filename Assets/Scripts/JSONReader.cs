﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JSONReader{
    private const string JSON_FILE_LOCATION = "Assets\\media.json";
    private string username;
    private string subPath;

    public MediaInfo Read(){
        var media_results = new MediaInfo();
        username = ModeSelectController.username;
        subPath = TourCatalogController.subPath;
        
        var json_data = GetJSON();

        InitMediaInfo(media_results, json_data);

        return media_results;
    }

    public JSONObject GetJSON()
    {
        username = ModeSelectController.username;
        subPath = TourCatalogController.subPath;
        string json_file_path = "";
    #if UNITY_EDITOR
        string current_directory = string.Format("{0}\\",Directory.GetCurrentDirectory());
        json_file_path = Path.Combine(current_directory,
            JSON_FILE_LOCATION);
    #else
        json_file_path = string.Format("/storage/emulated/0/Tours/{0}/{1}/media.json", username, subPath);
    #endif
        string json_content = File.ReadAllText(json_file_path);
        return new JSONObject(json_content);
    }

    public List<JSONObject> GetAllJSON()
    {
        username = ModeSelectController.username;
        List<JSONObject> json_content_list = new List<JSONObject>();
        string[] json_file_path_list;
#if UNITY_EDITOR
#else
#endif

        string root = string.Format("/storage/emulated/0/Tours/{0}", username);
        json_file_path_list = Directory.GetDirectories(root);
        foreach (var path in json_file_path_list)
        {
            string json_content = File.ReadAllText(string.Format("{0}/media.json", path));
            json_content_list.Add(new JSONObject(json_content));
            
        }
        return json_content_list;
    }

    private void InitMediaInfo(MediaInfo media, JSONObject jsonDataInfo){
        var json_media = jsonDataInfo.GetField(PropertyStrings.Media);
        var json_locations = json_media.GetField(PropertyStrings.MediaLocArr).list;

        media.id = (int)json_media.GetField(PropertyStrings.MediaId).n;
        media.title = json_media.GetField(PropertyStrings.MediaTitle).str;
        media.subPath = json_media.GetField(PropertyStrings.MediaSubPath).str;
        media.summary = json_media.GetField(PropertyStrings.MediaSummary).str;
        media.infoPanel = json_media.GetField(PropertyStrings.MediaInfo).b;
        media.previewImageUrl = json_media.GetField(PropertyStrings.MediaPreviewimg).str;
        media.locations = new List<LocationInfo>();

        foreach(var loc in json_locations){
            InitLocationInfo(media, loc);
        }
    }

    private void InitLocationInfo(MediaInfo media, JSONObject jsonLocationInfo)
    {
        var new_location = new LocationInfo();
        var json_objects = jsonLocationInfo.GetField(PropertyStrings.LocationObjectArr).list;

        new_location.id = (int)jsonLocationInfo.GetField(PropertyStrings.LocationId).n;
        new_location.title = jsonLocationInfo.GetField(PropertyStrings.LocationTitle).str;
        new_location.type = jsonLocationInfo.GetField(PropertyStrings.LocationType).str;
        new_location.subPath = jsonLocationInfo.GetField(PropertyStrings.LocationSubPath).str;
        new_location.locationFilePath = jsonLocationInfo.GetField(PropertyStrings.LocationImg).str;
        new_location.showTime = (int)jsonLocationInfo.GetField(PropertyStrings.LocationTime).n;
        new_location.objects = new List<ObjectInfo>();

        foreach(var obj in json_objects)
        {
            InitObjectInfo(new_location, obj);
        }

        media.locations.Add(new_location);
    }

    private void InitObjectInfo(LocationInfo loc, JSONObject jsonObjectInfo)
    {
        // TODO | Needs the proper button added (buttons are split into 2 objects right now)

        var new_object = new ObjectInfo();
        var json_position = jsonObjectInfo.GetField(PropertyStrings.ObjectPositionObj);
        
        new_object.label = jsonObjectInfo.GetField(PropertyStrings.ObjectLabel).str;
        new_object.iconTag = jsonObjectInfo.GetField(PropertyStrings.ObjectIcon).str;
        new_object.destinationId = jsonObjectInfo.GetField(PropertyStrings.ObjectDestination) 
            ? (int)jsonObjectInfo.GetField(PropertyStrings.ObjectDestination).n 
            : -1;
        new_object.description = jsonObjectInfo.GetField(PropertyStrings.ObjectDescription) 
            ? jsonObjectInfo.GetField(PropertyStrings.ObjectDescription).str 
            : null;
        new_object.imageFilePath = jsonObjectInfo.GetField(PropertyStrings.ObjectImg) 
            ? jsonObjectInfo.GetField(PropertyStrings.ObjectImg).str 
            : null;
        new_object.audioFilePath = jsonObjectInfo.GetField(PropertyStrings.ObjectAudio) 
            ? jsonObjectInfo.GetField(PropertyStrings.ObjectAudio).str 
            : null;

        InitPositionInfo(new_object, json_position);

        loc.objects.Add(new_object);
    }

    private void InitPositionInfo(ObjectInfo obj, JSONObject jsonPositionInfo)
    {
        var object_position = new PositionInfo();

        object_position.distance = jsonPositionInfo.GetField(PropertyStrings.PositionDistance).n;
        object_position.xOffset = jsonPositionInfo.GetField(PropertyStrings.PositionXOff).n;
        object_position.yOffset = jsonPositionInfo.GetField(PropertyStrings.PositionYOff).n;

        obj.position = object_position;
    }
}
