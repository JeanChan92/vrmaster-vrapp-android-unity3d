﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BouncyCastle.Utilities.Platform.Android;
using UnityEngine.SceneManagement;

public class PermissionChecker : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<PermissionProvider>().VerifyStorage(hasBeenGranted => OnGranted());

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnGranted()
    {
        SceneManager.LoadScene(1);
    }
}
