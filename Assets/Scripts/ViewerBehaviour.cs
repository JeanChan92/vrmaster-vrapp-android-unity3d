﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewerBehaviour
{
    public string GetLocationPath(LocationInfo loc)
    {
        return loc.locationFilePath;
    }
}
