﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TourInfoController : MonoBehaviour {

	public GameObject title;
	public GameObject summary;
	public GameObject image;
	public GameObject btPlay;
	public GameObject btDel;
    public Button btPlayu;
    public Text txtPlayu;

	private ITourObtainer obtainer;

	// Use this for initialization
	void Start () {
		// definition and attachment to obtainer
		
		if(ModeSelectController.online)
			obtainer = new TourInfoObtainerOnline();
		else
			obtainer = new TourInfoObtainerOffline();
		
		obtainer.Attach(this);
		obtainer.Start();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) 
			OnBackButtonClick();
	}

	public void OnPlayButtonClick()
	{
        // Obtainer either downloads or starts playing the tour
        btPlayu.interactable = false;
        txtPlayu.text = "Downloading...";
		obtainer.PlayTour();
	}

	public void OnDelButtonClick()
	{
		// Tells the obtainer to delete the tour folder and all inside it

		obtainer.DeleteTour();
	}

	public void OnBackButtonClick()
	{
		// Goes back to the catalog

		SceneManager.LoadScene(2);
	}

    internal void SetTitle(string title)
    {
		// Changes the title to that of the tour

		this.title.GetComponent<Text>().text = title;
    }

    internal void SetSummary(string summary)
    {
		// Changes the summary to that of the tour

		this.summary.GetComponent<Text>().text = summary;
    }

    internal void SetImage(Texture2D imageData)
    {
		// Changes the image to that of the tour's first location

		image.GetComponent<RawImage>().texture = imageData;
    }

    public void SetButtons(int buttonState)
    {
		// Sets up the buttons depending on whether the files tour is already downloaded or not

		switch(buttonState)
		{
			case 0: // Tour is Downloaded | Online Mode
                btPlayu.interactable = true;
				txtPlayu.text = "Play";
				btDel.SetActive(true);
				break;
			case 1: // Tour is not Downloaded | Online Mode
                btPlayu.interactable = true;
                txtPlayu.text = "Download";
				btDel.SetActive(false);
				break;
			case 2: // Tour is not Downloaded | Offline Mode
				OnBackButtonClick();
				break;
			default:
				break;
		}
    }
}
