﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITourObtainer{
	void Start();
	void Attach(TourInfoController controller);
	void GetTourInfo();
	void SetupButtons();
	void GetImage(JSONObject apiData);
	void GetTitle(JSONObject apiData);
	void GetSummary(JSONObject apiData);
	void PlayTour();
	void DeleteTour();
}
