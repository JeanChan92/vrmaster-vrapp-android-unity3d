﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TourInfoObtainerOffline : ITourObtainer
{
	private string username;
	private string subPath;
    private TourInfoController controller;

    public void Attach(TourInfoController controller)
    {
        this.controller = controller;
    }

    public void DeleteTour()
    {
        // Deletes the files and directory

    #if UNITY_EDITOR
		Debug.Log("Simulate Delete");
    #else
		string check_path = string.Format("/storage/emulated/0/Tours/{0}/{1}", username, subPath);
        Directory.Delete(check_path, true);
    #endif
        controller.SetButtons(2);
    }

    public void GetImage(JSONObject data)
    {
        // Converts the image from path and converts it to texture for the controller
        
        string imageURL = data.GetField("media").GetField(PropertyStrings.MediaPreviewimg).str;
        var texture = new Texture2D(4, 4);
        byte[] fileData = File.ReadAllBytes(imageURL);
        texture.LoadImage(fileData);

        controller.SetImage(texture);
    }

    public void GetSummary(JSONObject data)
    {
        // Gets the summary from the media file and notifies the controller

        string summary = data.GetField("media").GetField(PropertyStrings.MediaSummary).str;
        controller.SetSummary(summary);
    }

    public void GetTitle(JSONObject data)
    {
        // Retrieves the title from the API and notifies the controller

        string title = data.GetField("media").GetField(PropertyStrings.MediaTitle).str;
        controller.SetTitle(title);
    }

    public void GetTourInfo()
    {
		// Gets the JSONObject from media.json

		var reader = new JSONReader();
		var data = reader.GetJSON();

        GetTitle(data);
		GetSummary(data);
		GetImage(data);
    }

    public void PlayTour()
    {
        // Starts playing the Tour
		SceneManager.LoadScene(4);
    }

    public void SetupButtons()
    {
        // In offline mode, it is assumed that the tour is downloaded and ready to play

        controller.SetButtons(0);
    }

    public void Start()
    {
        // Start and Attach are the only methods that should be called from other classes

		username = ModeSelectController.username;
		subPath = TourCatalogController.subPath;

		GetTourInfo();
		SetupButtons();
    }
}
