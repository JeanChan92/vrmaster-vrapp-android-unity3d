﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using BestHTTP;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TourInfoObtainerOnline : ITourObtainer{

	private string username;
	private string subPath;
    private TourInfoController controller;
    private TourDownloader td;

	public void Start () {
        // Start and Attach are the only methods that should be called from other classes

		username = ModeSelectController.username;
		subPath = TourCatalogController.subPath;
        td = new TourDownloader();

        td.Attach(this);
        GetTourInfo();
		SetupButtons();
	}

    public void Attach(TourInfoController controller)
    {
        // Attached controller so the obatiner knows who to notify | Check: Design Pattern Observer
        this.controller = controller;
    }

    public void SetupButtons()
    {
        // Tells the controller how to setup the buttons
        int buttonState = FileExist() ? 0 : 1;
        controller.SetButtons(buttonState);
    }

    public void GetTourInfo()
    {
        // Sends request of obtaining the API information of the tour
        Debug.Log(subPath);
        var request = new HTTPRequest(new Uri(string.Format("https://vrapp.co/api/v1/vr-environments/vr-content-items?username={0}&env_url_subpath={1}", username, subPath)),
            OnAPIRequestFinished);

        request.Send();
    }

    private void OnAPIRequestFinished(HTTPRequest originalRequest, HTTPResponse response)
    {
        // Converts data to JSONObject and calls the methods to get all the information

		var apiData = new JSONObject(response.DataAsText);

		GetTitle(apiData);
		GetSummary(apiData);
		GetImage(apiData);
    }

    public void GetImage(JSONObject apiData)
    {
        // Gets the URL of the first location image and calls a request to access te image

        string url = apiData.GetField("item_data").list[0].GetField("default_url").str;

        var request = new HTTPRequest(new Uri(url), OnImageRequestFinished);
        request.AddHeader("referer", string.Format("https://vrapp.co/{0}", username));
        request.Send();
    }

    private void OnImageRequestFinished(HTTPRequest originalRequest, HTTPResponse response)
    {
        // Retrieves the image and notifies the controller

        var imageData = response.DataAsTexture2D;
        controller.SetImage(imageData);
    }

    public void GetSummary(JSONObject apiData)
    {
        // Retrieves the summary from the API and notifies the controller

        string summary = apiData.GetField("summary").str;
        controller.SetSummary(summary);
    }

    public void GetTitle(JSONObject apiData)
    {
        // Retrieves the title from the API and notifies the controller

        string title = apiData.GetField("title").str;
        controller.SetTitle(title);
    }

    public void PlayTour()
    {
        // Checks if files are present, else downloads images and and rest

        if(FileExist())
            SceneManager.LoadScene(4);
        else
            DownloadTour();
    }

    private void DownloadTour()
    {
        // Downloads all the files
        
        td.Run(username, subPath);
    }

    public void OnDownloadFinish()
    {
        // Tells the controller that the tour is now viewable
        controller.SetButtons(0);
    }

    private bool FileExist()
    {
        // Checks whether the directory of the tour already exists locally

    #if UNITY_EDITOR
		string check_path = Path.Combine(Application.persistentDataPath, "/VRmaster/VRmasternative/360Viewer/360Viewer/Assets/");
    #else
		string check_path = string.Format("/storage/emulated/0/Tours/{0}/{1}", username, subPath);
    #endif
		if(!Directory.Exists(check_path))
            return false;
		else
            return true;
    }

    public void DeleteTour()
    {
        // Deletes the files and directory

    #if UNITY_EDITOR
		Debug.Log("Simulate Delete");
    #else
		string check_path = string.Format("/storage/emulated/0/Tours/{0}/{1}", username, subPath);
        Directory.Delete(check_path, true);
    #endif
        controller.SetButtons(1);
    }
}
