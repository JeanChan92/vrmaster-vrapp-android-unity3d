﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediaInfo
{
	public int id;
	public string title;
    public string subPath;
	public string summary;
	public string previewImageUrl;
	public bool infoPanel;
	public List<LocationInfo> locations;
}
