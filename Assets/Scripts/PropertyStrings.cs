﻿using System.Collections.Generic;

public static class PropertyStrings
{
    public const string Media = "media";
    public const string MediaId = "id";
    public const string MediaTitle = "title";
    public const string MediaSubPath = "subpath";
    public const string MediaSummary = "summary";
    public const string MediaPreviewimg = "previewImageUrl";
    public const string MediaInfo = "infoPanel";
    public const string MediaLocArr = "locations";

    public const string LocationId = "id";
    public const string LocationTitle = "title";
    public const string LocationType = "type";
    public const string LocationSubPath = "subpath";
    public const string LocationImg = "imageFilePath";
    public const string LocationTime = "showTime";
    public const string LocationObjectArr = "objects";

    public const string ObjectPositionObj = "position";
    public const string ObjectLabel = "label";
    public const string ObjectDescription = "description";
    public const string ObjectIcon = "iconTag";
    public const string ObjectImg = "imageFilePath";
    public const string ObjectAudio = "audioFilePath";
    public const string ObjectDestination = "destinationId";

    public const string PositionDistance = "distance";
    public const string PositionXOff = "xOffset";
    public const string PositionYOff = "yOffset";
}
