﻿using HTC.UnityPlugin.Multimedia;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VR;

public class SceneController : MonoBehaviour
{
    public ViveMediaDecoder viveMediaDecoder;

    public GameObject sphere;
    public GameObject hotSpotPrefab;
    public GameObject audioPlayer;
    public Renderer sphereRenderer;
    public GameObject debugText;

    private const float TEXT_ROW_LIMIT = 0.075f;
    private const string HOTSPOT_TYPE_TRAVEL = "destination";
    private const string HOTSPOT_TYPE_DESCRIPTION = "description";
    private const string HOTSPOT_TYPE_AUDIO = "audio";

    private static ControlManager controlManager = new ControlManager();
    private static List<ObjectInfo> currentHotSpots = new List<ObjectInfo>();
    private static List<GameObject> shownHotSpots = new List<GameObject>();

    void Start() {
        VRSettings.enabled = true;
        controlManager.GetMedia();
        SetView(controlManager.GetLocation(0));
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            VRSettings.enabled = false;
            SceneManager.LoadScene(2);
        } 
    }

    private void SetView(LocationInfo currentLocation) {
        if (currentLocation.type == "photo") {
            SetImage(currentLocation.locationFilePath);
        }
        else if (currentLocation.type == "media") {
            SetVideo(currentLocation.locationFilePath);
        }
    }

    private void SetImage(string imageURL) {
        byte[] imageData = File.ReadAllBytes(imageURL);
        Texture2D location_texture = new Texture2D(4, 4);

        location_texture.LoadImage(imageData);
        sphereRenderer.material.mainTexture = location_texture;
        foreach (GameObject hotspot in shownHotSpots) {
            GameObject.Destroy(hotspot);
        }
        SetHotspots();
    }
    
    private void SetVideo(string videoURL) {
        viveMediaDecoder.mediaPath = videoURL;
        viveMediaDecoder.Run();
        foreach (GameObject hotspot in shownHotSpots) {
            GameObject.Destroy(hotspot);
        }
        SetHotspots();
    }

    private void ShowDescription(GameObject infoHotSpot) {
        infoHotSpot.transform.GetChild(0).gameObject.SetActive(true);
        infoHotSpot.transform.GetChild(1).transform.localScale *= 7;
        infoHotSpot.transform.GetChild(2).gameObject.SetActive(true);
    }

    private void PlayAudio (string audioFilePath) {
        AudioSource audio_source = audioPlayer.GetComponent<AudioSource>();
        string audio_url = audioFilePath.Substring(7, 13);
        StartCoroutine(LoadAudio(audio_source));
        if (audio_source.clip != null) {
            if (!audio_source.isPlaying) {
                audio_source.volume = 1.0f;
                audio_source.Play();
            }
        }
        else {
            SceneManager.LoadScene(1);
        }
    }

    private IEnumerator LoadAudio(AudioSource asource) {
        WWW load = new WWW("/storage/emulated/0/Tours/bhv/tour/Audio/audio11.mp3");
        yield return load;

        AudioClip clip = load.GetAudioClip(false, false);
        asource.clip = clip;
    }

    private void SetHotspots() {
        // Sets up all the hotspot onto the scene
        currentHotSpots = controlManager.GetHotSpots();
        shownHotSpots = new List<GameObject>();
        int objectList_count = currentHotSpots.Count;
        for (int i = 0; i < objectList_count; i++) {
            var new_hotspot = (GameObject)Instantiate(hotSpotPrefab);
            float offsetX = currentHotSpots[i].position.xOffset;
            float offsetY = currentHotSpots[i].position.yOffset;
            float posX = Mathf.Sin(offsetX * (Mathf.PI / 180f)) * 0.5f;
            float posY = Mathf.Sin(-offsetY * (Mathf.PI / 180f)) * 0.5f;
            float posZ = Mathf.Cos(offsetX * (Mathf.PI / 180f)) * 0.5f;

            Debug.Log(offsetX);
            Debug.Log(Mathf.Sin(offsetX));
            Debug.Log(posX);

            new_hotspot.transform.position = new Vector3(posX, posY, posZ);
            new_hotspot.transform.Rotate(new Vector3(offsetY, offsetX, 0));
            if (currentHotSpots[i].description != null) {
                new_hotspot.transform.GetChild(0).gameObject.SetActive(false);
                string description = currentHotSpots[i].description;
                string label = currentHotSpots[i].label;
                TextMesh tm = new_hotspot.transform.GetChild(0).GetComponent<TextMesh>();
                TextWrap(tm, description);
                TextMesh label_text_mesh = new_hotspot.transform.GetChild(2).GetComponent<TextMesh>();
                label_text_mesh.text = label;
            }
            else {
                new_hotspot.transform.GetChild(0).gameObject.SetActive(false);
                string description = "";
                TextMesh tm = new_hotspot.transform.GetChild(0).GetComponent<TextMesh>();

                TextWrap(tm, description);

                string label = currentHotSpots[i].label;
                TextMesh label_text_mesh = new_hotspot.transform.GetChild(2).GetComponent<TextMesh>();
                label_text_mesh.text = label;
            }
            new_hotspot.name = i.ToString();
            new_hotspot.transform.GetChild(2).gameObject.SetActive(true);
            shownHotSpots.Add(new_hotspot);
        }
    }

    public void OnHotspotOnGaze(GameObject targetHotSpot) {
        if (targetHotSpot.tag != "Finish") {
            ShowDescription(targetHotSpot.transform.parent.gameObject);
        }
    }

    public void OnHotSpotDeActivate(GameObject targetHotSpot) {
        // resets the hotspots
        int tag_number = targetHotSpot != null || targetHotSpot.tag != "Finish"
            ? System.Convert.ToInt32(targetHotSpot.transform.parent.name)
            : -1;
        var targetObject = tag_number != -1
            ? currentHotSpots[tag_number]
            : null;
        string hotspot_type = targetObject != null
            ? controlManager.GetHotSpotType(targetObject)
            : HOTSPOT_TYPE_TRAVEL;

        switch(hotspot_type){
            case HOTSPOT_TYPE_TRAVEL:
                HideDescription(targetHotSpot.transform.parent.gameObject);
                break;
            case HOTSPOT_TYPE_DESCRIPTION:
                HideDescription(targetHotSpot.transform.parent.gameObject);
                break;
            case HOTSPOT_TYPE_AUDIO:
                HideDescription(targetHotSpot.transform.parent.gameObject);
                StopAudio();
                break;
            default:
                break;
        }
    }

    private void StopAudio() {
        audioPlayer.GetComponent<AudioSource>().Stop();
    }

    private void HideDescription(GameObject infoHotSpot){
        if (infoHotSpot.transform.GetChild(0).gameObject.activeSelf) {
            infoHotSpot.transform.GetChild(1).transform.localScale /= 7;
            infoHotSpot.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    public void OnHotSpotActivate(GameObject targetHotSpot) {
        // Calls the method of to activate the hotspot depending on the type
        if (targetHotSpot.tag == "Finish") {
            VRSettings.enabled = false;
            SceneManager.LoadScene(2);
        }

        Debug.Log(targetHotSpot.name);
        int tag_number = System.Convert.ToInt32(targetHotSpot.transform.parent.name);
        var check = currentHotSpots[tag_number];
        KeyValuePair<string, string> kvp = controlManager.HotSpotActivate(check);

        switch (kvp.Key) {
            case HOTSPOT_TYPE_TRAVEL:
                SetImage(kvp.Value);
                break;
            case HOTSPOT_TYPE_DESCRIPTION:
                break;
            case HOTSPOT_TYPE_AUDIO:
                PlayAudio(kvp.Value);
                break;
            default:
                break;
        }
    }

    private void TextWrap(TextMesh targetTextMesh, string contentText) {
        // Fits the text in a certain box
        string[] parts = contentText.Split(' ');
        string tmp = "";
        targetTextMesh.text = "";
        for (int i = 0; i < parts.Length; i++) {
            tmp = targetTextMesh.text;

            targetTextMesh.text += parts[i] + " ";
            if (targetTextMesh.GetComponent<Renderer>().bounds.extents.x > TEXT_ROW_LIMIT) {
                tmp += System.Environment.NewLine;
                tmp += parts[i] + " ";
                targetTextMesh.text = tmp;
            }

        }
    }

}
