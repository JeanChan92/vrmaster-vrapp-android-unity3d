﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ModeSelectController : MonoBehaviour {

	public static string username;
	public static bool online;

	public GameObject textField;
	public GameObject errorText;
	public GameObject onlineButton;

	// Use this for initialization
	void Start () {
		username = "";
		if(CheckForInternet())
			onlineButton.GetComponent<Button>().interactable = true;
		else
			onlineButton.GetComponent<Button>().interactable = false;
        
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) 
    		Application.Quit(); 
	}

	public void OnExitClick()
	{
		Application.Quit();
	}

	public void OnOnlineClick()
	{
		online = true;
		OnClick();
	}

	public void OnOfflineClick()
	{
		online = false;
		OnClick();
	}

	private void OnClick()
	{
		// TO EDIT AND REMOVE
		errorText.SetActive(false);
		string usrName = textField.GetComponent<Text>().text;
		if(usrName != "")
		{
			if(online)
				CheckOnlineTour(usrName);
			else
				CheckOfflineTour(usrName);
		}
		else
		{
			errorText.SetActive(true);
		}
	}

	private void CheckOfflineTour(string user)
	{
#if UNITY_EDITOR
		string directory = "";
#else	// !UNITY_EDITOR
		string directory = string.Format("/storage/emulated/0/Tours/{0}", user);
#endif	// UNITY_EDITOR
		if(Directory.Exists(directory))
		{
			username = user;
			SceneManager.LoadScene(2);
		}
		else
		{
			errorText.SetActive(true);
		}
	}

	private void CheckOnlineTour(string user)
	{
		username = user;
		SceneManager.LoadScene(2);
	}

	private bool CheckForInternet()
	{
		try
		{
			using(var client = new WebClient())
			{
				using(client.OpenRead("http://clients3.google.com/generate_204"))
				{
					return true;
				}
			}
		}
		catch
		{
			return false;
		}
	}
}
