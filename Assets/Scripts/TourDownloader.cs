﻿using System;
using System.Collections.Generic;

using UnityEngine;
using BestHTTP;
using BestHTTP.Statistics;
using BestHTTP.Examples;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Threading;

using System.Security.AccessControl;
using System.Security.Principal;

public interface ILoadVRAssets
{
    void GetFromServer(string username);
    void StoreData(MediaInfo media);
}

public class TourDownloader : ILoadVRAssets
{

	private string userName;
    private string subPath;

    private const string PREVIEW_IMAGE_PATH = "Assets/Images/image0.jpg";
    private const string MEDIA_FILE_PATH = "/VRmaster/VRmasternative/360Viewer/360Viewer/Assets/media.json";
    private const string ANDROID_TOUR_PATH = "/storage/emulated/0/Tours";

    // PRIVATE FIELDS
    private int itemTotalCount;
    private int imageDownloadCount;
    private int videoDownloadCount;
    private int audioTotalCount;
    private int audioDownloadCount;
    private bool[] ready;

    private TourInfoObtainerOnline tioo;
    
    // Observer METHODS

    public void Run(string username, string subPath)
    {
        ready = new bool[3] {false, false, false};
        imageDownloadCount = 0;
        audioDownloadCount = 0;
        this.userName = username;
        this.subPath = subPath;
		GetFromServer(userName);
    }

    public void Attach(TourInfoObtainerOnline tioo)
    {
        this.tioo = tioo;
    }

    // ILoadVRAssets METHODS
    
    /// <summary>Gets All files and information from the Server</summary>
    /// <param name="username">Informs server which Tour information is needed</param>
    /// 
    public void GetFromServer(string username)
    {
        GetAPI(username);
    }

    /// <summary>Creates and stores all the filtered information</summary>
    /// <param name="media">Contains all the needed information from the server</param>
    /// 
    public void StoreData(MediaInfo media)
    {
        var jsonMedia = CreateMedia(media);

        if (CreateJSONFile(jsonMedia))
        {
            ready[2] = true;
            ReadyCheck();
        }
    }
    
    /// <summary>Starts the actual application</summary>
    /// 
    // public void LoadApplication()
    // {
    //     loadText.GetComponent<TextMesh>().text = "Starting Tour...";
    //     SceneManager.LoadScene(1);
    // }


    // CHECK METHODS

    /// <summary>Checks whether all downloads and files storing are done, then calls LoadApplication</summary>
    /// <see cref="LoadApplication"/>
    /// 
    private void ReadyCheck()
    {
        try
        {
            if (Array.TrueForAll(ready, delegate (bool x) { return x; }))
            {
                ready[0] = false;
                ready[1] = false;
                ready[2] = false;

                //LoadApplication();
                tioo.OnDownloadFinish();
            }
            else
                Debug.Log(string.Format("READY CHECK FAILED {0}, {1}, {2}", ready[0], ready[1], ready[2]));
        }
        catch
        {
            tioo.OnDownloadFinish();
        }
        
    }


    // DOWNLOAD METHODS
    
    /// <summary>Gets the data from the API</summary>
    /// <param name="username"></param>
    /// <returns>True if function has been called successfully</returns>
    /// <remarks>Calls the Callback method OnDataRequestFinished</remarks>
    /// 
    private bool GetAPI(string username)
    {
        var request = new HTTPRequest(new Uri(string.Format("https://vrapp.co/api/v1/vr-environments/vr-content-items?username={0}&env_url_subpath={1}", username, subPath)),
            OnAPIRequestFinished);

        request.Send();

        return true;
    }

    /// <summary>Calls the next methods after the data has been received from the API</summary>
    /// <param name="apiData">The raw json file received from the server</param>
    /// 
    private void ProcessAPI(JSONObject apiData)
    {
        var media = InitMedia(apiData);
        InitLocation(media, apiData);
        InitObject(media, apiData);
        
        

        GetImageFiles(apiData);
        GetAudioFiles(apiData);
        GetVideoFiles(apiData);


        StoreData(media);
    }

    /// <summary>Stores the image received from the server</summary>
    /// <param name="imageId">The id of the image file</param>
    /// <param name="imageData">The image data represented in a array of bytes</param>
    /// 
    private void ProcessImage(int imageId, byte[] imageData)
    {
        string savePath = "";

        imageDownloadCount++;

    #if UNITY_EDITOR
            savePath = Path.Combine(Application.persistentDataPath, string.Format("/VRmaster/VRmasternative/360Viewer/360Viewer/Assets/Images/image{0}.jpg", imageId));
    #else   // !UNITY_EDITOR 
           
            string path = string.Format("{0}/{1}/{2}/Images", ANDROID_TOUR_PATH, userName, subPath);
            if(!Directory.Exists(path))
            {   
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch(UnauthorizedAccessException e)
                {
                }
                catch(DirectoryNotFoundException e)
                {
                }
            }
            savePath = string.Format("{0}/image{1}.jpg", path, imageId);
    #endif  // UNITY_EDITOR
        File.WriteAllBytes(savePath, imageData);


        if (imageDownloadCount + videoDownloadCount == itemTotalCount)
        {
            ready[0] = true;
        }

        ReadyCheck();
    }

    private void ProcessVideo(int videoId, byte[] videoData)
    {
        string savePath = "";

        // Coded for Android use only!
        videoDownloadCount++;

        string path = string.Format("{0}/{1}/{2}/Video", ANDROID_TOUR_PATH, userName, subPath);
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        savePath = string.Format("{0}/video{1}.mp4", path, videoId);

        File.WriteAllBytes(savePath, videoData);

        if (videoDownloadCount + imageDownloadCount == itemTotalCount)
        {
            ready[0] = true;
        }

        ReadyCheck();
    }

    /// <summary>Stores the audio received from the server</summary>
    /// <param name="audioId">The id of the audio file</param>
    /// <param name="audioData">The audio data represented in array of bytes</param>
    /// 
    private void ProcessAudio(int audioId, byte[] audioData)
    {
        string savePath = "";
        
        // use try on download and then catch if something goes wrong, but add audioDownloadCount regardless
        audioDownloadCount++;
#if UNITY_EDITOR
        savePath = Path.Combine(Application.persistentDataPath, string.Format("/VRmaster/VRmasternative/360Viewer/360Viewer/Assets/Audio/audio{0}.mp3", audioId));
#else   // !UNITY_EDITOR
        string path = string.Format("{0}/{1}/{2}/Audio", ANDROID_TOUR_PATH, userName, subPath);
        if(!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        savePath = string.Format("{0}/audio{1}.mp3", path, audioId);
#endif  // UNITY_EDITOR
        File.WriteAllBytes(savePath, audioData);

        if (audioDownloadCount == audioTotalCount)
        {
            ready[1] = true;
        }

        ReadyCheck();
    }

    /// <summary>Gets the image file from the url path and calls the callback method to handle the server response</summary>
    /// <see cref="OnImageRequestFinished(HTTPRequest, HTTPResponse)"/>
    /// <param name="path">Source url of the image file</param>
    /// <param name="id">Number assigned to image file</param>
    /// 
    private void GetImageFromUrl(string path, int id)
    {
        var requestImage = new HTTPRequest(new Uri(path), OnImageRequestFinished);

        requestImage.AddHeader("referer", string.Format("https://vrapp.co/{0}", userName));
        requestImage.Tag = id;
        requestImage.Send();
    }

    private void GetVideoFromUrl(string path, int id)
    {
        var requestVideo = new HTTPRequest(new Uri(path), OnVideoRequestFinished);

        requestVideo.AddHeader("referer", string.Format("https://vrapp.co/{0}", userName));
        requestVideo.Tag = id;
        requestVideo.Send();
    }

    /// <summary>Gets the audio file from the url path and calls the callback method to handle the server response</summary>
    /// <see cref="OnAudioRequestFinished(HTTPRequest, HTTPResponse)"/>
    /// <param name="path">Source url from the audio file</param>
    /// <param name="id">Number assigned to audio file</param>
    /// <returns>Whether a source url has been passed through</returns>
    /// 
	private bool GetAudioFromUrl(string path, int id)
	{
        if (path != null)
        {
            HTTPRequest requestImage = new HTTPRequest(new Uri(path), OnAudioRequestFinished);
            requestImage.AddHeader("referer", string.Format("https://vrapp.co/{0}", userName));
            requestImage.Tag = id;
            requestImage.Send();

            return true;
        }

        return false;
	}
    
    /// <summary>Calls the method to request the images for each location</summary>
    /// <see cref="GetImageFromUrl(string, int)"/>
    /// <returns>If the method was run successfully</returns>
    /// 
    public bool GetImageFiles(JSONObject apiData)
    {
        var itemDataList = apiData.GetField("item_data").list;
        itemTotalCount = itemDataList.Count;

        for (int i = 0; i < itemTotalCount; i++)
        {
            if(itemDataList[i].GetField("media_type").str == "photo")
            {
                var imageUrl = itemDataList[i].GetField("default_url");
                GetImageFromUrl(imageUrl.str, i);
            }
        }

        return true;
    }

    public bool GetVideoFiles(JSONObject apiData)
    {
        var itemDataList = apiData.GetField("item_data").list;
        itemTotalCount = itemDataList.Count;

        for (int i = 0; i < itemTotalCount; i++)
        {
            if (itemDataList[i].GetField("media_type").str == "video")
            {
                var videoUrl = itemDataList[i].GetField("original_url");
                GetVideoFromUrl(videoUrl.str, i);
            }
        }

        return true;
    }

    /// <summary>Calls the method to request the audios for each object</summary>
    /// <see cref="GetImageFromUrl(string, int)"/>
    /// <returns>If the method was run successfully</returns>
    /// 
    public bool GetAudioFiles(JSONObject apiData)
    {
        var itemDataList = apiData.GetField("item_data").list;
        int itemDataCount = itemDataList.Count;
        List<int[,]> audioPositions = new List<int[,]>();

        for (int i = 0; i < itemDataCount; i++)
        {
            var objectsList = itemDataList[i].GetField("pois").list;
            int objectsCount = objectsList.Count;

            for (int j = 0; j < objectsCount; j++)
            {
                var audioPath = objectsList[j].GetField("audio");

                if (audioPath != null)
                {
                    int[,] audioPos = { {i, j} };
                    audioPositions.Add(audioPos);
                }
            }
        }

        audioTotalCount = audioPositions.Count;
        for (int i = 0; i < audioPositions.Count; i++)
        {
            var audioPath = new JSONObject();
            int objIndex = audioPositions[i][0, 0];

            int audioIndex = audioPositions[i][0, 1];

            audioPath = itemDataList[objIndex].GetField("pois").list[audioIndex].GetField("audio");

            GetAudioFromUrl(audioPath.str, (objIndex * 10) + audioIndex);
        }

        return true;
    }


    // STORING INFORMATION METHODS

    /// <summary>Stores the received information from the server into a MediaStruct object</summary>
    /// <param name="apiData">The raw json file received from the server</param>
    /// <returns>The MediaStruct with the neccessary information in it</returns>
    /// <remarks>Does not contain the location information yet</remarks>
    /// 
    private MediaInfo InitMedia(JSONObject apiData)
    {
        var media = new MediaInfo();

        media.id = 0;
        media.title = apiData.GetField("short_title").str;
        media.subPath = apiData.GetField("url_subpath").str;
        media.summary = apiData.GetField("summary").str;
        media.infoPanel = false;
        media.previewImageUrl = string.Format("{0}/{1}/{2}/Images/image0.jpg", ANDROID_TOUR_PATH, userName, subPath);
        media.locations = new List<LocationInfo>();
        
        return media;
    }

    /// <summary>Stores the received information from the server into a LocationStruct and adds it to the already created MediaStruct</summary>
    /// <param name="media">The MediaStruct to which the LocationStruct(s) are going to be added to</param>
    /// <param name="apiData">The raw json file received from the server</param>
    /// <remarks>Does not contain the object information yet</remarks>
    /// 
    private void InitLocation(MediaInfo media, JSONObject apiData)
    {
        var locList = apiData.GetField("item_data").list;
        int locCount = locList.Count;

        for (int i = 0; i < locCount; i++)
        {
            var locData = locList[i];
            var loc = new LocationInfo();

            loc.id = i;
            loc.title = locData.GetField("short_title").str;
            loc.type = locData.GetField("media_type").str;
            loc.subPath = locData.GetField("url_subpath").str;
    #if UNITY_EDITOR
            loc.locationFilePath = string.Format("Assets/Images/image{0}.jpg", i);
    #else // !UNITY_EDITOR
            loc.locationFilePath = string.Format("{0}/{1}/{2}/Images/image{3}.jpg", ANDROID_TOUR_PATH, userName, subPath, i);
    #endif // UNITY_EDITOR
            loc.showTime = 0;
            loc.objects = new List<ObjectInfo>();

            media.locations.Add(loc);
        }
    }

    /// <summary>Stores the received information from the server into a ObjectStruct and adds it to the already created LocationStruct</summary>
    /// <param name="media">The MediaStruct containing the LocationStruct(s) to which the ObjectStruct(s) are going to be added to</param>
    /// <param name="apiData">The raw json file received from the server</param>
    /// <remarks>This method calls InitPosition to receive the right position for the objects</remarks>
    /// <see cref="InitPosition(JSONObject)"/>
    /// 
    private void InitObject(MediaInfo media, JSONObject apiData)
    {
        var locList = apiData.GetField("item_data").list;
        var locCount = locList.Count;

        for (int i = 0; i < locCount; i++)
        {
            var objList = locList[i].GetField("pois").list;
            var objCount = objList.Count;

            for (int j = 0; j < objCount; j++)
            {
                var objData = objList[j];
                var objMain = new ObjectInfo();
                var pos = objData.GetField("position");
                var choices = objData.GetField("choices");
                int posIdMain = choices.list.Count > 0 ? 0 : -1;
                string linkDestination = objData.GetField("link") == null 
                    ? null 
                    : objData.GetField("link").GetField("bubble").str;
                string iconTag = linkDestination == null && posIdMain == -1 
                    ? "info" 
                    : "arrow-up";
                string imageFilePath = linkDestination == null 
                    ? "Images/panel.jpg" 
                    : null;
                string label = objData.GetField("title") == null 
                    ? null 
                    : objData.GetField("title").str;
                string description = objData.GetField("description") 
                    ? objData.GetField("description").str 
                    : null;
                string audioFilePath = objData.GetField("audio")
#if UNITY_EDITOR 
                    ? string.Format("Assets/Audio/audio{0}{1}.mp3", i, j)
#else // !UNITY_EDITOR
                    ? string.Format("{0}/{1}/{2}/Audio/audio{3}{4}.mp3", ANDROID_TOUR_PATH, userName, subPath, i, j)
#endif // UNITY_EDITOR
                    : null;

                label = choices.list.Count > 0 
                    ? description
                    : label;
                linkDestination = choices.list.Count > 0 
                    ? choices.list[0].GetField("link").GetField("bubble").str 
                    : linkDestination;
                LocationInfo dest = media.locations.Find(x => x.subPath == linkDestination);
                string destination_label = media.locations.Find(x => x.subPath == linkDestination) == null
                    ? null
                    : media.locations.Find(x => x.subPath == linkDestination).title;

                objMain.label =  label ?? destination_label;
                objMain.description = choices.list.Count > 0
                    ? choices.list[0].GetField("text").str
                    : description;
                objMain.destinationId = linkDestination == null && choices.list.Count < 1 
                    ? -1 
                    : dest.id;
                objMain.imageFilePath = imageFilePath;
                objMain.audioFilePath = audioFilePath;
                objMain.iconTag = iconTag;
                objMain.position = InitPosition(posIdMain, pos);

                media.locations[i].objects.Add(objMain);

                if (choices.list.Count > 0)
                {
                    var objSecond = new ObjectInfo();
                    int posIdSecond = 1;

                    label = "";
                    linkDestination = choices.list[1].GetField("link").GetField("bubble").str;
                    dest = media.locations.Find(x => x.subPath == linkDestination);

                    objSecond.label = label;
                    objSecond.destinationId = dest.id;
                    objSecond.description = choices.list[1].GetField("text").str;
                    objSecond.iconTag = iconTag;
                    objSecond.position = InitPosition(posIdSecond, pos);

                    media.locations[i].objects.Add(objSecond);
                }
            }
        }
        
    }

    /// <summary>Stores the received information from the server into a PositionStruct</summary>
    /// <param name="posData">The raw data of the Object position</param>
    /// <returns>The newly formated position information</returns>
    /// 
    private PositionInfo InitPosition(int posId, JSONObject posData)
    {
        var pos = new PositionInfo();

        pos.distance = 1;
        pos.yOffset = posData.GetField("theta").n;
        switch (posId)
        {
            case -1:
                pos.xOffset = -posData.GetField("gamma").n;
                break;
            case 0:
                pos.xOffset = -posData.GetField("gamma").n-5;
                break;
            case 1:
                pos.xOffset = -posData.GetField("gamma").n+5;
                break;
            default:
                break;
        }

        return pos;
    }


    // STORE INFORMATION INTO JSONOBJECT

    /// <summary>Creates the media JSONObject containing the filtered information from the MediaStruct</summary>
    /// <param name="media">The MediaStruct containing the information of the media on the server</param>
    /// <returns>The media JSONObject</returns>
    /// <remarks>Calls the CreateLocation method and contains the entire information of the media object</remarks>
    /// <see cref="CreateLocation(int, LocationInfo)"/>
    /// 
    public JSONObject CreateMedia(MediaInfo media)
    {
        var jMediaObject = new JSONObject(JSONObject.Type.OBJECT);
        var locList = new JSONObject(JSONObject.Type.ARRAY);
        int locCount = media.locations.Count;

        jMediaObject.AddField(PropertyStrings.MediaId, media.id);
        jMediaObject.AddField(PropertyStrings.MediaTitle, media.title);
        jMediaObject.AddField(PropertyStrings.MediaSubPath, media.subPath);
        jMediaObject.AddField(PropertyStrings.MediaSummary, media.summary);
        jMediaObject.AddField(PropertyStrings.MediaInfo, media.infoPanel.ToString());
        jMediaObject.AddField(PropertyStrings.MediaPreviewimg, media.previewImageUrl);

        for (int i = 0; i < locCount; i++)
        {
            locList.Add(CreateLocation(i, media.locations[i]));
        }

        jMediaObject.AddField(PropertyStrings.MediaLocArr, locList);

        return jMediaObject;
    }

    /// <summary>Creates the location JSONObject containing the filtered information from the LocationStruct</summary>
    /// <param name="id">Identifier for the location</param>
    /// <param name="loc">The LocationStruct found in the MediaStruct from CreateMedia</param>
    /// <returns>The location JSONObject</returns>
    /// <remarks>Calls the CreateObject method and contains the entire information of the location object</remarks>
    /// <see cref="CreateMedia(MediaInfo)"/>
    /// <see cref="CreateObject(ObjectInfo)"/>
    /// 
    public JSONObject CreateLocation(int id, LocationInfo loc)
    {
        var jLocationObject = new JSONObject(JSONObject.Type.OBJECT);
        var objList = new JSONObject(JSONObject.Type.ARRAY);
        int objCount = loc.objects.Count;

        jLocationObject.AddField(PropertyStrings.LocationId, loc.id);
        jLocationObject.AddField(PropertyStrings.LocationTitle, loc.title);
        jLocationObject.AddField(PropertyStrings.LocationType, loc.type);
        jLocationObject.AddField(PropertyStrings.LocationSubPath, loc.subPath);
        jLocationObject.AddField(PropertyStrings.LocationImg, loc.locationFilePath);
        jLocationObject.AddField(PropertyStrings.LocationTime, loc.showTime);

        for (int i = 0; i < objCount; i++)
        {
            objList.Add(CreateObject(loc.objects[i]));
        }

        jLocationObject.AddField(PropertyStrings.LocationObjectArr, objList);

        return jLocationObject;
    }

    /// <summary>Creates the object JSONObject containing the filtered information from the ObjectStruct</summary>
    /// <param name="obj">The ObjectStruct found in the LocationStruct from CreateLocation</param>
    /// <returns>The object JSONObject</returns>
    /// <remarks>Calls the CreatePosition method and contains the entire information of the object object</remarks>
    /// <see cref="CreateLocation(int, LocationInfo)"/>
    /// <see cref="CreatePosition(PositionInfo)"/>
    /// 
    public JSONObject CreateObject(ObjectInfo obj)
    {
        var jObjectObject = new JSONObject(JSONObject.Type.OBJECT);

        if (obj.label != null)
            jObjectObject.AddField(PropertyStrings.ObjectLabel, obj.label);
        if (obj.description != null)
            jObjectObject.AddField(PropertyStrings.ObjectDescription, obj.description);
        if (obj.destinationId != -1)
        {
            jObjectObject.AddField(PropertyStrings.ObjectDestination, obj.destinationId);
        }
        else
        {
            jObjectObject.AddField(PropertyStrings.ObjectImg, obj.imageFilePath);
        }
        if (obj.audioFilePath != null)
            jObjectObject.AddField(PropertyStrings.ObjectAudio, obj.audioFilePath);
        jObjectObject.AddField(PropertyStrings.ObjectIcon, obj.iconTag);
        jObjectObject.AddField(PropertyStrings.ObjectPositionObj, CreatePosition(obj.position));

        return jObjectObject;
    }

    /// <summary>Creates the position JSONObject containing the filtered information from the PositionStruct</summary>
    /// <param name="pos">The PositionStruct found in the ObjectStruct from CreateObject</param>
    /// <returns>The position JSONObject</returns>
    /// <see cref="CreateObject(ObjectInfo)"/>
    /// 
    public JSONObject CreatePosition(PositionInfo pos)
    {
        var jPosObject = new JSONObject(JSONObject.Type.OBJECT);

        jPosObject.AddField(PropertyStrings.PositionDistance, pos.distance);
        jPosObject.AddField(PropertyStrings.PositionXOff, pos.xOffset);
        jPosObject.AddField(PropertyStrings.PositionYOff, pos.yOffset);

        return jPosObject;
    }

    /// <summary>Creates a .json file containing the information of the media JSONObject</summary>
    /// <param name="media">The JSONObject containing all the filtered information of the API</param>
    /// <returns>If the method was run successfully</returns>
    /// 
    public bool CreateJSONFile(JSONObject media)
    {
        var jsonObject = new JSONObject(JSONObject.Type.OBJECT);
        string jsonOutput;
        string savePath = "";

        jsonObject.AddField(PropertyStrings.Media, media);
        jsonOutput = jsonObject.Print(true);

#if UNITY_EDITOR
            savePath = Path.Combine(Application.persistentDataPath, MEDIA_FILE_PATH);
#else   // !UNITY_EDITOR
            
            string path = string.Format("/storage/emulated/0/Tours/{0}/{1}", userName, subPath);
            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            savePath = string.Format("{0}/media.json", path);
            
#endif  // UNITY_EDITOR
        File.WriteAllText(savePath, jsonOutput);

        return true;
    }


    // CALLBACK METHODS

    /// <summary> Handles the data it recieves from the Server</summary>
    /// <param name="request">Points to the request sender</param>
    /// <param name="response">The information returned from the server</param>
    /// 
    void OnAPIRequestFinished(HTTPRequest request, HTTPResponse response)
    {
        var apiData = new JSONObject(response.DataAsText);

        ProcessAPI(apiData);
    }

    /// <summary>Handles the image files recieved from the server</summary>
    /// <param name="request">Reference to the request sender</param>
    /// <param name="response">Data received from the server</param>
    /// 
    void OnImageRequestFinished(HTTPRequest request, HTTPResponse response)
    {

        var responseImage = response.DataAsTexture2D.EncodeToJPG();
        int imageId = (int)request.Tag;

        ProcessImage(imageId, responseImage);
    }

    void OnVideoRequestFinished(HTTPRequest request, HTTPResponse response)
    {
        int videoId = (int)request.Tag;
        if (response != null)
        {
            var responseVideo = response.Data;

            ProcessVideo(videoId, responseVideo);
        }
        else
            Debug.Log("EMPTY RESPONSE DETECTED");
    }

    /// <summary>Handles the audio files received from the server</summary>
    /// <param name="request">Reference to the request sender</param>
    /// <param name="response">Data received from the server</param>
    /// 
    void OnAudioRequestFinished(HTTPRequest request, HTTPResponse response)
    {
        int audioId = (int)request.Tag;
        if (response != null)
        {
            var responseAudio = response.Data;

            ProcessAudio(audioId, responseAudio);
        }
        else
            Debug.Log("EMPTY RESPONSE DETECTED");
    }
}
