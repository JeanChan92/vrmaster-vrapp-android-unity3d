﻿using System.Collections.Generic;

public class PositionInfo
{
    public float distance;
    public float xOffset;
    public float yOffset;
}
