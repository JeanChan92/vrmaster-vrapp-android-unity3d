﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VR;
using BestHTTP;
using System;

// Gets all tours based on username and sets up the prefabs for the catalog
public class TourCatalogController : MonoBehaviour{

	public static string subPath;

	public GameObject titleText;
	public GameObject itemPrefab;
	public GameObject itemCatalog;

	private ITourCatalogObtainer obtainer; 


    // Use this for initialization
    void Start () {
		VRSettings.LoadDeviceByName("cardboard");
		VRSettings.enabled = false;

		if(ModeSelectController.online)
			obtainer = new CatalogObtainerOnline();
		else
			obtainer = new CatalogObtainerOffline();

		obtainer.Attach(this);
		SetTitle();
		obtainer.Start();
	}

    void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
   			OnBackButtonClick();
	}

    private void SetTitle()
    {
        string username = ModeSelectController.username;

		titleText.GetComponent<Text>().text = username;
    }

    internal void SetSubPath(string subPathName)
    {
        subPath = subPathName;
    }

    internal void InitPrefab(string title, int i)
    {
        var tourItem = (GameObject)Instantiate(itemPrefab, itemCatalog.transform, false);
		tourItem.GetComponentInChildren<Text>().text = title;
		tourItem.GetComponent<Button>().onClick.AddListener(() => OnTourClick(i));
    }

	public void OnTourClick(int index)
	{
		obtainer.OnTourClick(index);
	}

    public void OnBackButtonClick()
	{
		SceneManager.LoadScene(1);
	}
}
