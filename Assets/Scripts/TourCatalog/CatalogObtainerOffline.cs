﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CatalogObtainerOffline : ITourCatalogObtainer
{
    private string username;
	private TourCatalogController controller;
    private List<string> titleList;
    private List<string> subpathList;
    
    public void Attach(TourCatalogController controller)
    {
        this.controller = controller;
    }

    public void GetSubPaths(JSONObject data)
    {
        string subpath = data.GetField(PropertyStrings.MediaSubPath).str;
        subpathList.Add(subpath);
    }

    public void GetTitles(JSONObject data)
    {

        string title = data.GetField(PropertyStrings.MediaTitle).str;
        titleList.Add(title);
    }

    public void GetTours()
    {
        var reader = new JSONReader();
        var jsonList = reader.GetAllJSON();
        

        foreach (var json in jsonList)
        {
            var data = json.GetField(PropertyStrings.Media);
            GetTitles(data);
            GetSubPaths(data);
        }
        InitPrefabs();
    }

    public void InitPrefabs()
    {
        for (int i = 0; i < titleList.Count; i++)
        {
            controller.InitPrefab(titleList[i], i);
        }
    }

    public void OnTourClick(int index)
    {
        SelectTour(index);
    }

    public void SelectTour(int tourIndex)
    {
        controller.SetSubPath(subpathList[tourIndex]);

        SceneManager.LoadScene(3);
    }

    public void Start()
    {
        username = ModeSelectController.username;
        titleList = new List<string>();
        subpathList = new List<string>();

        GetTours();
    }
}
