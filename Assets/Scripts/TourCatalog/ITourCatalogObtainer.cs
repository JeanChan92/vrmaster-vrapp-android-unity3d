﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITourCatalogObtainer{
	void Attach(TourCatalogController controller);
	void Start();
	void GetTours();
	void GetTitles(JSONObject data);
	void GetSubPaths(JSONObject data);
	void InitPrefabs();
	void SelectTour(int tourIndex);
    void OnTourClick(int index);
}
