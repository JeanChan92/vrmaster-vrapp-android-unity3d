﻿using System;
using System.Collections;
using System.Collections.Generic;
using BestHTTP;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CatalogObtainerOnline : ITourCatalogObtainer
{
	private string username;
	private List<string> subPathList;
	private List<string> tourTitles;
	private TourCatalogController controller;

    public void Attach(TourCatalogController controller)
    {
        this.controller = controller;
    }

    public void GetTitles(JSONObject data)
    {
		var toursData = data.GetField("vr_environments");
		var tourList = toursData.list;

		foreach(JSONObject tour in tourList)
		{
            try
            {
                string title = tour.GetField("short_title").str;
                tourTitles.Add(title);
            }
            catch (Exception)
            {
            }
		}
    }

    public void GetSubPaths(JSONObject data)
    {
        var toursData = data.GetField("vr_environments");
		var tourList = toursData.list;

		foreach(JSONObject tour in tourList)
		{
            string sub = tour.GetField("url_subpath").str;
            subPathList.Add(sub);
		}
    }

    public void GetTours()
    {
        var request = new HTTPRequest(new Uri(string.Format("https://vrapp.co/api/v1/vr-environments?owner_username={0}", username)),
            OnAPIRequestFinished);

        request.Send();
    }

    private void OnAPIRequestFinished(HTTPRequest originalRequest, HTTPResponse response)
    {
		var data = new JSONObject(response.DataAsText);

        GetTitles(data);
		GetSubPaths(data);
		InitPrefabs();
    }

    public void InitPrefabs()
    {
        for(int i = 0; i < tourTitles.Count; i++)
		{
			controller.InitPrefab(tourTitles[i], i);
		}
    }

    public void SelectTour(int tourIndex)
    {
        controller.SetSubPath(subPathList[tourIndex]);
		SceneManager.LoadScene(3);
    }

    public void Start()
    {
		username = ModeSelectController.username;
		tourTitles = new List<string>();
		subPathList = new List<string>();
        GetTours();
    }

    public void OnTourClick(int index)
    {
        SelectTour(index);
    }
}
