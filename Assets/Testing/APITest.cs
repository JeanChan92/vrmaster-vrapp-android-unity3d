﻿using BestHTTP;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class APITest : MonoBehaviour {

    public Text debugText;
    public string username;

	// Use this for initialization
	void Start () {
        GetAPI();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void GetAPI()
    {
        var request = new HTTPRequest(new Uri(string.Format("https://vrapp.co/api/v1/vr-environments?owner_username={0}", username)),
            OnAPIRequestFinished);

        request.Send();
    }

    private void OnAPIRequestFinished(HTTPRequest originalRequest, HTTPResponse response)
    {
        debugText.text = response.DataAsText;

        var data = new JSONObject(response.DataAsText);

        GetTitles(data);
    }

    private void GetTitles(JSONObject data)
    {
        var toursData = data.GetField("vr_environments");
        var tourList = toursData.list;
        debugText.text = "";
        foreach (var tour in tourList)
        {
            try
            {

                debugText.text += tour.GetField("title").str;
            }
            catch (Exception)
            {
            }
        }
        
    }
}
